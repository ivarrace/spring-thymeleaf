package com.ivarrace.api.service;

import com.ivarrace.api.model.Usuario;
import com.ivarrace.api.repository.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        /**SQL: (pass: 123456)
         INSERT INTO usuario(id,email,nombre,password) VALUES (1,'user@mail.com', 'user','$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.');
         INSERT INTO usuario_roles(usuario_id,roles) VALUES (1,'ROLE_USER');
         */
        Optional<Usuario> usr = usuarioRepository.findByNombre(username);
        if (!usr.isPresent()) {
            String error = "User not found by name: " + username;
            LOGGER.error(error);
            throw new UsernameNotFoundException(error);
        }

        List<SimpleGrantedAuthority> auths = usr.get().getRoles().stream()
                .map(rol -> new SimpleGrantedAuthority(rol))
                .collect(Collectors.toList());
        return new User(usr.get().getNombre(), usr.get().getPassword(), auths);
    }
}